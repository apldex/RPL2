package pertemuan1;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author apldex
 */

/*
//  Bentuk umum deklarasi class pada Java
//  <modifier> class [nama class] { //statement }
//
//  Catatan:
//      Nama class harus sama dengan nama file
//      Besar kecil huruf pada nama file dan class berpengaruh
//      Penulisan nama class mengikuti kaidah PascalCase (upper camel case)
//
//  Informasi lebih lanjut tentang access modifier:
//      https://docs.oracle.com/javase/tutorial/java/javaOO/accesscontrol.html
//      https://www.tutorialspoint.com/java/java_access_modifiers.html
 */
public class Product {

    /*
    //  Bentuk umum deklarasi attribute/variable pada Java
    //  <modifier> [tipe data/class] [nama attribute/variable] ;
    //
    //  Penulisan nama attribute/variable mengikuti kadiah camelCase(lower camel case)
     */

    public String id;
    private String name;
    private int price;

    /*
    //  Constructor pada Java diberi nama sesuai dengan nama class dan tidak mempunyai return type
    //  Constructor secara otomatis dipanggil saat object diinstansiasi.
     */
    public Product() {
        this.id = "0000";
        this.name = "null";
        this.price = 0;
    }

    /*
    //  Bentuk umum deklarasi method pada Java
    //  <modifier> [tipe pengembalian] [nama method](<tipe data> <parameter1>, <tipe data> <parameterN>) { //statement }
    //
    //  Penulisan nama method mengikuti kadiah camelCase(lower camel case)
     */
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }
}
