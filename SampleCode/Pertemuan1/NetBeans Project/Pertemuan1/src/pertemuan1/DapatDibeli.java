/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pertemuan1;

/**
 *
 * @author apldex
 */

/*
//  Bentuk umum deklarasi interface pada Java
//  <modifier> interface [nama interface] { //statement }
//
//  Catatan:
//      Nama class harus sama dengan nama file
//      Besar kecil huruf pada nama file dan interface berpengaruh
//      Penulisan nama interface mengikuti kaidah PascalCase (upper camel case)
//      Interface hanya berisi method abstract (method tanpa implementasi)
//      Interface tidak dapat diinstansiasi
//
 */

public interface DapatDibeli {
    public void stokBerkurang(int jumlah);
}
