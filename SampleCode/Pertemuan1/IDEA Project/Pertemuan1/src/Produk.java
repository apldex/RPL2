/**
 * Created by apldex on 4/23/17.
 */

/*
//  Untuk implementasi sebuah interface,
//  tambahkan keyword implements setelah nama class.
//  Contoh:
//
//  public class NamaClass implements NamaInterface { //statements }
 */

public class Produk implements DapatDibeli {

    private String kode = "1001";
    private String nama = "Laptop" ;
    private int harga = 2000;
    private int stok = 10;

    /*
    //  Saat sebuah class mengimplementasi interface,
    //  seluruh method pada interface tersebut harus diimplementasi
    //  jika tidak, compiler akan menampilkan pesan error
     */

    @Override
    public void stokBerkurang(int jumlah) {
        this.stok = this.stok - jumlah;
    }

    public void printProduk(){
        System.out.println("Kode: "+ kode + "\nNama: " + nama + "\nHarga: " + harga + "\nStok: " + stok);
    }

    public static void main(String[] args) {
        Produk produk = new Produk();
        produk.stokBerkurang(5);
        produk.printProduk();
    }

}
