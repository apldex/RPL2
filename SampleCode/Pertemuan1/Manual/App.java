/**
 * Created by apldex on 4/23/17.
 */

/*
//  Main Class dengan nama App
//
//  Class ini yang akan dijalankan terlebih dahulu saat aplikasi dijalankan
//  Class App memuat main method
 */
public class App {

    /*
    // --- Main Method ---
    //
    // Pada Main Method, terdapat instansiasi object dari class Product
    // serta pengaksesan method dan attribute dari class Product
    // Main method akan dijalan saat aplikasi dijalankan
     */

    public static void main(String[] args) {

        /*
        //  Instansiasi sebuah object dapat dilakukan dengan dua cara
        //  Pertama, deklarasi object terlebih dahulu lalu instansiasi.
        //  Kedua, instansiasi langsung saat object tersebut diinisiasi seperti berikut:
        //
        //  NamaClass namaObject = new NamaClass();
        //
         */

        Product product; //deklarasi object product dari class Product
        product = new Product(); //instansiasi object product

        product.id = "1029";
        product.setName("Smartphone");
        product.setPrice(1000);

        System.out.println("Product id: " + product.getId() +
                            "\nProduct name: " + product.getName() +
                            "\nProduct price: " + product.getPrice());
    }
}
