/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package labti.model;

/**
 *
 * @author apldex
 */

public class Seller extends BaseModel implements CanSell {
    private String address;
    private String email;
    private String phoneNumber;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    /*
    //  Method yang dioverride dari interface CanSell
     */
    @Override
    public void sell(Product product) {
        System.out.println("Seller " + name + " sells " + product.getName());
    }
}
