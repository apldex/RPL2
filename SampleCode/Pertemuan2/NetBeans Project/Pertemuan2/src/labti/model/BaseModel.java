/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package labti.model;

/**
 *
 * @author apldex
 */

/*
//  Class BaseModel merupakan parent class model
//  Class ini memiliki attribute umum yang dimiliki model lain, seperti id dan nama
//  Class ini bersifat abstract, sehingga tidak dapat diinstansiasi
 */
public abstract class BaseModel {
    /*
    //  Attribute id dan name dideklarasi sebagai protected
    //  agar attribute tersebut hanya dapat diakses oleh subclassnya saja
     */
    protected String id;
    protected String name;

    /*
    //  Penggunaan method getter dan setter adalah bentuk dari enkapsulasi
    //  Hal ini dilakukan data pada attribute tidak mudah diubah
     */
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
