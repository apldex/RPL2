/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package labti.app;

/**
 *
 * @author apldex
 */

/*
//  Class App harus mengimpor class Customer, Product dan Seller agar dapat
//  menggunakan class tersebut
//  karena berada pada package yang berbeda, yaitu package labti.model.
//  Sedangkan, class App berada pada package labti.app.
 */
import labti.model.Customer;
import labti.model.Product;
import labti.model.Seller;

public class App {
    public static void main(String[] args){
        Customer customer = new Customer();
        Seller seller = new Seller();
        Product product = new Product();

        //Set id dan name untuk object customer, seller dan product
        customer.setId("10000");
        customer.setName("Adrian");
        product.setId("20000");
        product.setName("MacBook Pro");
        seller.setId("30000");
        seller.setName("Faisal");

        customer.boughtProduct(product); //customer membeli satu product
        customer.boughtProduct(product, 3); //customer membeli sejumlah product

        seller.sell(product); //seller menjual product
        customer.sell(product); //customer menjual product
    }
}
