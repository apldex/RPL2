package labti.model;

/**
 * Created by apldex on 4/24/17.
 */
public interface CanSell {
   void sell(Product product);
}
