package labti.model;

/**
 * Created by apldex on 4/24/17.
 */

/*
//  Class Customer merupakan subclass dari class BaseModel
//  Sehingga class Customer memiliki seluruh attribute dan method non-private
//  milik class BaseModel
//
//  Ini adalah salah satu bentuk dari Inheritance.
//  Inheritance ditandai dengan keyword extends setelah nama class
 */
public class Customer extends BaseModel implements CanSell{
    private String address;
    private String email;
    private String phoneNumber;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    /*
    //  Method boughtProduct() merupakan method yang dioverload
     */
    public void boughtProduct(Product product){
        System.out.println(name + " buys a " + product.getName());
    }

    public void boughtProduct(Product product, int quantity) {
        System.out.println(name + " buy "+ quantity +" " + product.getName() + "(s)");
    }

    /*
    //  Method yang dioverride dari interface CanSell
     */
    @Override
    public void sell(Product product) {
        System.out.println("Customer " + name + " sells " + product.getName());
    }
}
