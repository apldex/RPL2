package labti.model;

/**
 * Created by apldex on 4/24/17.
 */
public class Product extends BaseModel {
    private int price;
    private int stock;

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }
}
